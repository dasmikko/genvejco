# genvej.co - De bedste kortlinks og statistik!
genvej.co er et website til at lave lange besværlige link til korte og nemme links!

Test server version: 1.1.0
Live version: 1.1.0

# Regler for de forskellige brugere:

## Gæst

- Kun tilfældig navngivet kortlinks

## Alm. brugere

- Max 5 navngivet links (Dog mulighed for at købe sig til flere)
- Max 1 link med statistik
- Kan ikke efterfølgende rette kortlinkets URL

## Premium

- Max 50 (Måske mere?) navngivet links (Dog mulighed for at købe sig til flere)
- Statistik for alle sine links (Også andres?)
  - Visninger
  - Browser
  - Hvor brugeren kom fra
  - Graf oversigt over visninger og browsere
- Mulighed for at ændre URL på et kortlink

# App

Der er jo tale om at lave en app, som giver mulighed for at kunne lave kortlinks fra, og synkronisere dem man har lavet på sitet. 